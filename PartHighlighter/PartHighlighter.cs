﻿/******************************************************************************
 *                     Part Highlighter for Kerbal Space Program              *
 *                                                                            *
 * Version 1.0 (first release)                                                *
 * Author: xEvilReeperx                                                       *
 * Created: 9/15/2013                                                         *
 * ************************************************************************** *
 * Code licensed under the terms of GPL v3.0                                  *
 *                                                                            *
 * See the included LICENSE.txt or visit http://www.gnu.org/licenses/gpl.html *
 * for the full license text.                                                 *
 *                                                                            *
 * The shader utilized by this plugin is NOT covered by this license; it is   *
 * free for personal and commercial use.                                      *
 *   The original shader is here:                                             *
 *      http://www.photonworkshop.com/index.php/blog/x-ray-shader/            *
 *                                                                            *
 * ***************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace PartHighlighter
{
    /// <summary>
    /// Contains actual Xraying functions and material that will be shared 
    /// among Part Renderers
    /// </summary>
    [KSPAddon(KSPAddon.Startup.EditorAny, false)]
    public class PartHighlighter : MonoBehaviour
    {
        
        static Material highlightMaterial;              // this material's shader will be applied to parts to be xrayed
        static Color highlightColor = new Color(0f, 1f, 0f, 1f);

        struct PreviousMaterial
        {
            public Shader shader;
            public Color color;

            public PreviousMaterial(Shader s, Color c)
            {
                shader = s;
                color = c;
            }
        }

        // this dictionary will be used to restore shaders and colors to parts
        static Dictionary<Part, List<PreviousMaterial>> materialDict = new Dictionary<Part, List<PreviousMaterial>>();

        /// <summary>
        /// Called just before the script is about to start receiving updates
        /// </summary>
        public void Start()
        {
            LoadSettings();
            CreateMaterial();

            GameEvents.onPartAttach.Add(OnPartAttach);  // works
            //GameEvents.onPartRemove.Add(OnPartRemove);  // works
            //GameEvents.onVesselChange.Add(OnVesselChanged); // doesn't work
            //GameEvents.onVesselWasModified.Add(OnVesselModified); // doesn't work
            //GameEvents.onPartDestroy.Add(OnPartDestroy); // doesn't seem to work, didn't look too hard though
            //GameEvents.onVesselLoaded.Add(OnVesselLoaded); // doesn't work
            //GameEvents.onGameSceneLoadRequested.Add(OnSceneChange); // works, but highlighter is destroyed between scenes,
                                                                      // even editor -> load ship -> editor scenes, so unnecessary

            StartCoroutine(CreateScripts()); // it seems that some parts don't have their staging icons ready at this point,
                                             // so we'll wait till next frame before attaching the xray scripts to parts of a
                                             // freshly loaded craft
        }


        private IEnumerator CreateScripts()
        {
            yield return new WaitForFixedUpdate();

            foreach (Part part in EditorLogic.fetch.ship.parts)
                AttachScript(part);
        }

        /// <summary>
        /// Create the Xray material that will be used
        /// </summary>
        private static void CreateMaterial()
        {
            Messager.Log("Creating outline material");

            highlightMaterial = new Material(global::PartHighlighter.Properties.Resources.OutlineShaderContents);
            if (!highlightMaterial)
            {
                Messager.LogError("Failed to create material.");
            }
            else Messager.Log("Created material.");
        }


        /// <summary>
        /// Should be pretty obvious
        /// </summary>
        private static void DestroyMaterial()
        {
            if (highlightMaterial)
                Destroy(highlightMaterial);
        }

        public void OnDestroy()
        {
            // restore any xrayed parts.  Just in case
            foreach(Part part in materialDict.Keys)
            {
                Unxray(part);
            }
        }


        /// <summary>
        /// Whenever the editor attaches a part to another, we'll take the
        /// opportunity to add the xray script.  If needed.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPartAttach(GameEvents.HostTargetAction<Part, Part> eventData)
        {
            if (eventData.host)
                AttachScript(eventData.host);

            // OnPartAttach isn't fired for the root part, so if it happens to
            // need a script we'll attach now
            if (EditorLogic.fetch.ship.parts.Count > 0)
                if (eventData.target == EditorLogic.fetch.ship.parts[0])
                    AttachScript(EditorLogic.fetch.ship.parts[0]);
        }


        /// <summary>
        /// Attaches the xray script to specified part, as long as it has a 
        /// staging icon and no previously attached script.
        /// </summary>
        /// <param name="part"></param>
        private void AttachScript(Part part)
        {
            if (part.GetComponent<HighlightScript>() != null || part.GetComponents<HighlightScript>().Length > 0)
            {
                //Messager.Log("Part " + part.ConstructID + " already has a highlight script.");
            }
            else
            {
                // add scripts only to parts with a staging icon
                if (part.hasStagingIcon && part.stackIcon != null)
                {
                    part.gameObject.AddComponent<HighlightScript>();
                    Messager.Log("Added highlight script to part '" + part.ConstructID + "'");
                }
            }
        }


        /// <summary>
        /// Applies Xray material shader to given part
        /// </summary>
        /// <param name="part"></param>
        public static void Xray(Part part) 
        {
            if (materialDict.ContainsKey(part))
                return; // part is already xrayed

            List<PreviousMaterial> oldMaterials = new List<PreviousMaterial>(); 

            // mesh could consist of multiple submeshes (one for each material), so we're
            // actually potentially dealing with several different materials here
            Renderer[] renderers = part.FindModelComponents<Renderer>();

            if (renderers.Length > 0)
            {
                for (int i = 0; i < renderers.Length; ++i)
                {
                    oldMaterials.Insert(i, new PreviousMaterial(renderers[i].sharedMaterial.shader, renderers[i].sharedMaterial.GetColor("_Color")));
                    renderers[i].sharedMaterial.shader = highlightMaterial.shader;
                    renderers[i].sharedMaterial.SetColor("_Color", highlightColor);
                }

                materialDict.Add(part, oldMaterials);
            }
            else
            {
                Messager.LogError("Part '" + part.ConstructID + "' has no renderers attached; cannot xray");
                Destroy(part.GetComponent<HighlightScript>());
            }
        }


        /// <summary>
        /// Restores original shader to part
        /// </summary>
        /// <param name="part"></param>
        public static void Unxray(Part part)
        {
            List<PreviousMaterial> oldMaterials = materialDict[part];

            if (oldMaterials.Count == 0)
                return; // part isn't xrayed

            Renderer[] renderers = part.FindModelComponents<Renderer>();

            if (renderers.Length > 0)
            {
                for (int i = 0; i < renderers.Length; ++i) {
                    renderers[i].sharedMaterial.shader = oldMaterials[i].shader;
                    renderers[i].sharedMaterial.SetColor("_Color", oldMaterials[i].color);
                        // it's probably very unlikely that we'd run into issues even if we didn't reset
                        // _Color, but it's so simple to avoid anyway that ...
                }
            }
            else
            {
                Messager.LogError("Part '" + part.ConstructID + "' has no renderers attached; cannot unxray");
                Destroy(part.GetComponent<HighlightScript>());
            }

            materialDict.Remove(part);
        }


        private void LoadSettings()
        {
            ConfigNode config = ConfigNode.Load(KSPUtil.ApplicationRootPath + "GameData/PartHighlighter/settings.cfg");

            if (config == null)
            {
                Messager.LogError("Failed to load config. Using default color");
            }
            else
            {
                var strColor = config.GetValue("xrayColor");

                if (string.IsNullOrEmpty(strColor))
                {
                    Messager.LogError("Did not find an \"xrayColor\" entry in config!");
                } else highlightColor = ParseColor(config.GetValue("xrayColor"));
            }
        }

        private static Color ParseColor(string str)
        {
            // "RGBA(0, 1, 0, 1)"
            //  0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
            //  R G B A ( 0 , _ 1 ,  _  0  ,  _  1  )

            string values = str.Substring(str.IndexOf('(') + 1, str.IndexOf(')') - str.IndexOf('(') - 1);

            string[] colors = values.Split(',');

            try {
                Color newColor = new Color(float.Parse(colors[0]), float.Parse(colors[1]), float.Parse(colors[2]), colors.Length == 4 ? float.Parse(colors[3]) : 1.0f);

                return newColor;
            } catch (ArgumentNullException) {}
            catch (FormatException) {}
            catch (OverflowException) {}

            Messager.LogError("Failed to parse '" + str + '\'');
            return Color.green;
        }

    } // end PartHighlighter class


    class HighlightScript : MonoBehaviour
    {
        #region member variables
            bool xrayed = false;
            Part part;
            bool wasReset = false;
        #endregion


        // --------------------------------------------------------------------
        //    Implementation
        // --------------------------------------------------------------------

        public void OnDestroy()
        {
            Unxray();
        }

        public void Start()
        {
            part = Part.FromGO(gameObject);

            if (!part)
                Messager.LogError("Failed to locate Part associated with this script");
        }


        /// <summary>
        /// Check conditions to see whether this part needs the Xray shader
        /// or not
        /// </summary>
        public void Update()
        {
            if (!HighLogic.LoadedSceneIsEditor)
            {
                //Messager.LogError("Part '" + part.ConstructID + "' has an attached HighlightScript outside of editor; removing it.");
                Destroy(this);
                return;
            }
            
            if (part)
                if (part.hasStagingIcon && part.stackIcon != null)
                {
                    // bug: for some reason, highlightIcon isn't reset correctly
                    //      when a grouped staging icon is expanded.  We'll look
                    //      for the start of a selection and set it back to a
                    //      consistent state
                    if (Staging.Selection.Count > 0)
                    {
                        if (!wasReset)
                        {
                            //Debug.Log("Resetting xray");
                            
                            part.stackIcon.highlight(false);
                            wasReset = true;
                            return;
                        }
                    }
                    else wasReset = false;

                    if (part.stackIcon.highlightIcon && Staging.FindIcon(part) != null)
                    {
                        if (!Staging.hover)
                        {
                            // no xray for this part
                            Unxray();
                        }
                        else
                        {
                            Xray();
                        }

                    }
                    else
                    {
                        Unxray();
                    }
                }
        }


        public void Unxray()
        {
            if (xrayed)
                PartHighlighter.Unxray(part);

            xrayed = false;

        }


        private void Xray()
        {
            if (!xrayed && part)
            {
                PartHighlighter.Xray(part);
                xrayed = true;
            }
        }
    } // end HighlightScript class



    class Messager
    {
        public static void Log(string message)
        {
            UnityEngine.Debug.Log("PartHighlighter: " + message);
        }

        public static void LogError(string message)
        {
            UnityEngine.Debug.Log("PartHighlighter: " + message);
        }
    }
}
